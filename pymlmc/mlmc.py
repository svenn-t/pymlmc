import numpy as np
from scipy import linalg
import warnings
import logging


# Set up logger for module and add a null handler so that we won't get an error if the main program hasn't set up a log
log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


class WeakConvergenceFailure(UserWarning):
    pass


def mlmc(l_min, l_max, n0, eps, mlmc_fn, alpha_0, beta_0, gamma):
    """
    Multi-level Monte Carlo estimation.

    (p, nl) = mlmc(...)

    Outputs:
      p:  value
      nl: number of samples on each level
    Inputs:
      l_min: minimum level of refinement  >= 2
      l_max: maximum level of refinement  >= l_min
      n0:   initial number of samples    >  0
      eps:  desired accuracy (rms error) >  0

      alpha: weak error is  O(2^{-alpha*l})
      beta:  variance is    O(2^{-beta*l})
      gamma: sample cost is O(2^{gamma*l})  > 0

      If alpha, beta are not positive then they will be estimated.

      mlmc_fn: the user low-level routine. Its interface is
        sums = mlmc_fn(l, n)
      with inputs
        l = level
        n = number of paths
      and a numpy array of outputs
        sums[0] = sum(Y)
        sums[1] = sum(Y**2)
      where Y are iid samples with expected value
        E[P_0]            on level 0
        E[P_l - P_{l-1}]  on level l > 0
    """
    # Check arguments
    if l_min < 2:
        raise ValueError("Need l_min >= 2")
    if l_max < l_min:
        raise ValueError("Need l_max >= l_min")
    if n0 <= 0 or eps <= 0 or gamma <= 0:
        raise ValueError("Need n0 > 0, eps > 0, gamma > 0")
    log.info('Starting MLMC with l_min = %d, eps = %5.5f, alpha = %5.5f, beta = %5.5f, and gamma = %5.5f', l_min, eps,
             alpha_0, beta_0, gamma)

    # Initialisation
    alpha = max(0, alpha_0)
    beta = max(0, beta_0)
    theta = 0.25
    lrun = l_min
    nl = np.zeros(lrun + 1)
    suml = np.zeros((2, lrun + 1))
    dnl = n0 * np.ones(lrun + 1)
    samp_f = [None for _ in range(lrun + 1)]
    samp_c = [None for _ in range(lrun + 1)]

    while sum(dnl) > 0:
        # update sample sums
        for l in range(0, lrun + 1):
            if dnl[l] > 0:
                sums, samples_fine, samples_coarse = mlmc_fn(l, int(dnl[l]))
                nl[l] += dnl[l]
                suml[0, l] += sums[0]
                suml[1, l] += sums[1]
                if samp_f[l] is None:
                    samp_f[l] = samples_fine.tolist()
                else:
                    samp_f[l].extend(samples_fine.tolist())
                if samp_c[l] is None:
                    samp_c[l] = samples_coarse.tolist()
                else:
                    samp_c[l].extend(samples_coarse.tolist())

        # compute absolute average and variance
        ml = np.abs(suml[0, :] / nl)
        vl = np.maximum(0, (suml[1, :] / nl) - (ml ** 2))

        # fix to cope with possible zero values for ml and vl
        # (can happen in some applications when there are few samples)
        for l in range(3, lrun + 2):
            ml[l-1] = max(ml[l - 1], (0.5 * ml[l - 2]) / (2 ** alpha))
            vl[l-1] = max(vl[l - 1], (0.5 * vl[l - 2]) / (2 ** beta))

        # use linear regression to estimate alpha, beta if not given
        if alpha_0 <= 0:
            a = np.ones((lrun, 2))
            a[:, 0] = range(1, lrun + 1)
            x, _ = linalg.lstsq(a, np.log2(ml[1:]))[0]
            alpha = max(0.5, -x)
            log.info('alpha not inputted. Using least squares estimation = %5.5f', alpha)

        if beta_0 <= 0:
            a = np.ones((lrun, 2))
            a[:, 0] = range(1, lrun + 1)
            x, _ = linalg.lstsq(a, np.log2(vl[1:]))[0]
            beta = max(0.5, -x)
            log.info('beta not inputted. Using least squares estimation = %5.5f', beta)

        # set optimal number of additional samples
        cl = 2 ** (gamma * np.arange(0, lrun + 1))
        ns = np.ceil(np.sqrt(vl / cl) * sum(np.sqrt(vl * cl)) / ((1 - theta) * eps ** 2))
        dnl = np.maximum(0, ns - nl)

        # if (almost) converged, estimate remaining error and decide
        # whether a new level is required
        if sum(dnl > 0.01 * nl) == 0:
            rem = ml[lrun] / ((2.0 ** alpha) - 1.0)

            if rem > np.sqrt(theta) * eps:
                log.info('Weak convergence tolerance not met. \n'
                         'Remaining error is %5.5f, while the tolerance is %5.5f', rem, np.sqrt(theta) * eps)
                if lrun == l_max:
                    warnings.warn("Failed to achieve weak convergence! Returning None", WeakConvergenceFailure)
                    return None, None, None, None, None, None
                else:
                    lrun += 1
                    vl = np.append(vl, vl[-1] / (2.0 ** beta))
                    nl = np.append(nl, 0.0)
                    suml = np.column_stack([suml, [0, 0]])
                    samp_f.append(None)
                    samp_c.append(None)

                    cl = 2 ** (gamma * np.arange(0, lrun + 1))
                    ns = np.ceil(np.sqrt(vl / cl) * sum(np.sqrt(vl * cl)) / ((1 - theta) * eps ** 2))
                    dnl = np.maximum(0, ns - nl)
                    log.info('A new level has been added (L = %d).', lrun)

    # finally, evaluate the multilevel estimator
    p = sum(suml[0, :] / nl)

    return p, nl, suml, dnl, samp_f, samp_c
