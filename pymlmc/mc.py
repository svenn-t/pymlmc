# Installed
import numpy as np
import logging


# Set up logger for module and add a null handler so that we won't get an error if the main program hasn't set up a log
log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


def monte_carlo(mc_fn, n0, eps, max_iter, **kwargs):
    """
    Run Monte Carlo simulations until either a mean-squared error tolerance or max. samples/iterations have been
    reached. 

    Input:
        - mc_fn:            The user low-level routine. Returns samples s, shape=(nsamples, )
        - n0:               Initial number of samples/iterations.
        - eps:              Desired MS error
        - max_iter:         Max. number of Monte Carlo simulations before we call it quits

    Output:
        - m_est:            Mean estimator
        - ms:               MS/variance of the mean estimator (OBS: NOT variance of samples)
        - iteration:        No. of iterations/samples used
        - stop_msg:         Message why we stopped simulation
    --------------------------------------------------------------------------------------------------------------------
    Interface of mc_fn is simple:

        s = mc_fn(n),

    where s is samples of function and n is no. samples to run.
    """
    # Check if there are optional input
    if 'save_every_n0_runs' in kwargs and kwargs['save_every_n0_runs'] == 'yes':
        save_n0_runs = True
    else:
        save_n0_runs = False

    # Check that n <= max_iter
    assert n0 <= max_iter, 'You have set number of initial iterations (n0) higher than max. number of iterations (' \
                           'max_iter)'

    # Setup
    n = n0  # no. of samples to run initially (in the continuation n is number of additional samples to run)
    iteration = 0  # no. of iterations/samples we have run so far (set initially to zero and add n in loop)
    if 'restart_sample' in kwargs:
        s = kwargs['restart_sample']
    else:
        s = np.empty(0)  # samples output (we append new samples as we run them)

    # Run Monte Carlo simulation until MS error tolerance or max. iter. have been met
    while n > 0:
        # Update iterations run so far
        iteration += n

        # Run simulator
        sample = mc_fn(n)
        s = np.append(s, sample)

        # Save (if needed)
        if save_n0_runs is True:
            np.savez('temp_save_mc_run.npz', s=s)

        # Evaluate error which for a variable X is ms = var / n where var = E[X**2] - E[X]**2 with E being sample
        # version of expectation
        ms = ((s ** 2).mean() - s.mean() ** 2) / s.size

        if ms >= eps:
            # Need additional samples. Naively, we just run n0 additional samples (if this comes over max_iter,
            # we just run as many samples as there are left until we meet max. iter)
            n = n0 if (iteration + n0) <= max_iter else (max_iter - iteration)
            log.info('No convergence! MS = %5.5f , while eps = %5.5f', ms, eps)
            log.info('Running %d additional samples. (If this is 0, we stop.)', n)
            log.info('Total number of runs so far is %d', iteration)
        else:
            log.info('Convergence achieved! MS = %5.5f, while eps = %5.5f', ms, eps)
            break

    # Check why we stopped
    stop_msg = 'Mean-squared error reached!' if ms < eps else 'Max. iterations reached!'

    return s.mean(), ms, iteration, stop_msg, s
