from pymlmc import mlmc_test, mlmc_plot
import matplotlib.pyplot as plt
import numpy as np


class CallType(object):
    def __init__(self, name, M, N, L, Eps):
        self.name = name
        self.M = M # refinement cost factor
        self.N = N # samples for convergence tests
        self.L = L # levels for convergence tests
        self.Eps = Eps


calltypes = [CallType("European", 4, 2000000, 5, [0.005, 0.01, 0.02, 0.05, 0.1]),
             CallType("Asian",    4, 2000000, 5, [0.005, 0.01, 0.02, 0.05, 0.1]),
             CallType("Lookback", 4, 2000000, 5, [0.01, 0.02, 0.05, 0.1, 0.2]),
             CallType("Digital",  4, 3000000, 5, [0.02, 0.05, 0.1, 0.2, 0.5])]


def opre_gbm(l, N, calltype, randn=np.random.randn):
    M = calltype.M  # refinement factor

    T = 1.0  # interval
    r = 0.05
    sig = 0.2
    K = 100.0

    nf = M ** l
    hf = T / nf

    nc = max(nf / M, 1)
    hc = T / nc

    sums = np.zeros(6)

    for N1 in range(1, N+1, 10000):
        N2 = min(10000, N - N1 + 1)

        X0 = K
        Xf = X0 * np.ones(N2)
        Xc = X0 * np.ones(N2)

        Af = 0.5 * hf * Xf
        Ac = 0.5 * hc * Xc

        Mf = np.array(Xf)
        Mc = np.array(Xc)

        if l == 0:
            dWf = np.sqrt(hf) * randn(N2)
            Xf += (r * Xf * hf) + (sig * Xf * dWf)
            Af += (0.5 * hf * Xf)
            Mf = np.minimum(Mf, Xf)
        else:
            for n in range(int(nc)):
                # dWc = np.zeros((1, N2))
                dWc = np.zeros(N2)

                for m in range(M):
                    dWf = np.sqrt(hf) * randn(N2)
                    dWc += dWf
                    Xf = (1.0 + r * hf) * Xf + (sig * Xf * dWf)
                    Af += (hf * Xf)
                    Mf = np.minimum(Mf, Xf)

                Xc += (r * Xc * hc) + (sig * Xc * dWc)
                Ac += (hc * Xc)
                Mc = np.minimum(Mc, Xc)

            Af -= (0.5 * hf * Xf)
            Ac -= (0.5 * hc * Xc)

        if calltype.name == "European":
            Pf = np.maximum(0, Xf - K)
            Pc = np.maximum(0, Xc - K)
        elif calltype.name == "Asian":
            Pf = np.maximum(0, Af - K)
            Pc = np.maximum(0, Ac - K)
        elif calltype.name == "Lookback":
            beta = 0.5826  # special factor for offset correction
            Pf = Xf - (Mf * (1 - (beta * sig * np.sqrt(hf))))
            Pc = Xc - (Mc * (1 - (beta * sig * np.sqrt(hc))))
        elif calltype.name == "Digital":
            Pf = K * 0.5 * (np.sign(Xf - K) + 1)
            Pc = K * 0.5 * (np.sign(Xc - K) + 1)

        Pf *= np.exp(-r * T)
        Pc *= np.exp(-r * T)

        if l == 0:
            Pc = 0

        sums += np.array([np.sum(Pf - Pc),
                          np.sum((Pf - Pc) ** 2),
                          np.sum((Pf - Pc) ** 3),
                          np.sum((Pf - Pc) ** 4),
                          np.sum(Pf),
                          np.sum(Pf ** 2)])
    return np.array(sums)


if __name__ == "__main__":
    N0 = 1000  # initial samples on coarse levels
    Lmin = 2  # minimum refinement level
    Lmax = 6  # maximum refinement level

    for (i, calltype) in enumerate(calltypes):
        def opre_l(l, N):
            return opre_gbm(l, N, calltype)
        filename = "opre_gbm%d.txt" % (i+1)
        logfile = open(filename, "w")
        mlmc_test(opre_l, calltype.M, calltype.N, calltype.L, N0, calltype.Eps, Lmin, Lmax, logfile)
        del logfile
        mlmc_plot(filename, nvert=3)
        plt.savefig(filename.replace('.txt', '.eps'))
