from setuptools import setup

setup(
    name='pymlmc',
    version='1.0',
    packages=['pymlmc'],
    dependencies=['numpy', 'scipy', 'matplotlib'],
    url='https://bitbucket.org/pefarrell/pymlmc/',
    license='',
    author='Patrick Farrell',
    author_email='',
    description='Python Multilevel Monte Carlo',
    zip_safe=True
)
